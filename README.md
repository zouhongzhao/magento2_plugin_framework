# Magento2 插件框架

#### 基本信息

这是m2的插件框架，要开发插件时,直接git clone下来进行修改即可。
包含的功能有：
* 1，前后台路由
* 2, 前台控制器
* 3, 插件首页Layout/Block/Phtml
* 4, Helper
* 5, Setup数据库升级文件
* 6，i18n翻译包

